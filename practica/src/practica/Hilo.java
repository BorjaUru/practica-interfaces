package practica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.collections.map.HashedMap;
/**
 * hilo que recorre el array para hacer las copias
 * @author borja
 *
 */
public class Hilo extends Thread {
	private ConcurrentHashMap<Copia,Copia>copias;;
	private String ruta;
	private Ventana v;
	String pass;
	String us;
	public Hilo(ConcurrentHashMap<Copia,Copia>copias,String ruta,String pss,String us){
		this.us=us;
		this.copias=copias;
		this.ruta=ruta;
		pass=pss;
	}
	/**
	 * parsea el dia en numero a letra
	 * @param dia
	 * @return
	 */
	public String dia(int dia){
		String diaa="";
		switch(dia){
			case 1:
				diaa="D";
				break;
			case 2:
				diaa="L";
				break;
			case 3:
				diaa="M";
				break;
			case 4:
				diaa="X";
				break;
			case 5:
				diaa="J";
				break;
			case 6:
				diaa="V";
				break;
			case 7:
				diaa="S";
				break;
		}
		return diaa;
	}
	/**
	 * metodo principal
	 */
	@Override
	public void run() {
		
		while(true){
			
			Iterator<Copia> it = copias.values().iterator();
	        while (it.hasNext()) {
	            Copia co=it.next();
	    	   String tiempo [] =co.getHora().split(":");
	    	   Calendar c=Calendar.getInstance();
	    	   GregorianCalendar g = new GregorianCalendar();
	    	   int num = g.get(GregorianCalendar.DAY_OF_WEEK);
	    	   String dia = dia(num);
	    	   int hora= c.get(Calendar.HOUR_OF_DAY);
	    	   int min = c.get(Calendar.MINUTE);
	    	   int horac=Integer.parseInt(tiempo[0]);
	    	   int minc=Integer.parseInt(tiempo[1]);
	    	   int mes=c.get(Calendar.MONTH);
	    	   int diaa=c.get(Calendar.DATE);
	    	   int ano=c.get(Calendar.YEAR);
	    	   if(Integer.parseInt(co.getFecha().split(" ")[0])==diaa&&Integer.parseInt(co.getFecha().split(" ")[1])==mes&&Integer.parseInt(co.getFecha().split(" ")[2])==ano&&co.getDia().equalsIgnoreCase("Diarias")){
	    		   
	    	   }
	    	   else if((co.getDia().equalsIgnoreCase("Diarias")||dia.equals(co.getDia()))&&horac==hora && minc==min){
	    		   Base b =new Base(ruta,pass,us);
	    		   b.copia(co.getBase());
	    		   if(!co.getDia().equals("Diarias")){
	    			   copias.remove(co);
	    			  new Fichero().wf(copias);
	    			   
	    		   }else{
	    			   co.setFecha(diaa+" "+mes+" "+ano);
	    		   }
	    		   
	    	   }
	       }
		}
	}

}
