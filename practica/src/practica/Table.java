package practica;
/**
 * define el objeto de la tabla
 * @author borja
 *
 */
public class Table {
	private String nombre;
	private float size;
	public Table(String nombre,float size){
		this.nombre=nombre;
		this.size=size;
	}
	public String getNombre() {
		return nombre;
	}
	public float getSize() {
		return size;
	}
	
}
