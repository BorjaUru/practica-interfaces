package practica;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import java.util.concurrent.ConcurrentHashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.swixml.SwingEngine;

import net.sf.jasperreports.engine.JRException;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.view.JasperViewer;



/**
 * Clase principal del programa que contiene el main y pinta la ventana
 * @author borja
 *
 */

public class Ventana extends JFrame{
	private ConcurrentHashMap<Copia,Copia> copiasS;
	private JMenuBar barra;
	private JMenuItem conectar;
	private JMenuItem configurar;
	private DefaultTableModel model;
	private JPanel sur;
	private JScrollPane  js;
	private JTable t;
	private String us;
	private String pss;
	private Hilo h;
	private String ruta;
	private ArrayList<String> b;
	private List<Tablee>listaCopias;
	private JButton bi;
	private Connection c;
	/**
	 * Contructor de la clase que genera la ventana
	 * @throws Exception
	 */
	public Ventana()throws Exception{
		try{
			new SwingEngine(this).render("practica.xml").setVisible(true);
			ruta=new Fichero().leerPropertis();
			
			conectar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					boolean a =false;
					while(!a){
						Login l=new Login();
						String ip=l.getIp();
						us=l.getUser();
						pss=l.getPass();
						Sql sql=new Sql();
						a=sql.conectar(ip,us,pss);
						c=sql.getC();
					}
					conectar.setEnabled(false);
					b=new Sql().listado(c);
					
					crearGraficos();
					configurar.setEnabled(true);
					
					bi.setEnabled(true);
					
					for(String j :b){
						String [] v={j};
						model.addRow(v);
					}
					copias();
					Base b=new Base(ruta, pss,us);
					b.crearCarpeta("");
					
				}
			});
			configurar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
				
					Configuracion co=new Configuracion(ruta,b,copiasS,h);
					ruta=co.getRutaa();
					new Fichero().crearModificarPropertis(ruta);
				
				}
			});
			
			copiasS=new Fichero().rf();
			JComboBox<String> br=new JComboBox<>();
			JButton bc=new JButton("Crear copia");
			bc.setEnabled(false);
			bc.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					Base b=new Base(ruta, pss,us);
					b.copia(model.getValueAt(t.getSelectedRow(), 0).toString());
					String copia=model.getValueAt(t.getSelectedRow(), 0).toString();
					File f = new File(ruta+copia+File.separator);
					br.removeAllItems();
					if (f.exists()){
						File[] ficheros = f.listFiles();
						if(ficheros.length<=0)
							br.addItem("No existen copias");
						br.addItem("Seleccionar Copia");
						for (int x=0;x<ficheros.length;x++){
							br.addItem(ficheros[x].getName());
						}
					}
					else {
						
						br.addItem("No existen copias");
					}
				}
			});
			
			br.setEnabled(false);
			br.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					String copia=br.getSelectedItem().toString();
					if(copia.equals("No existen copias")||copia.equals("Seleccionar Copia")){
						
					}else{
						Base b=new Base(ruta, pss,us);
						b.restaurarCopia(model.getValueAt(t.getSelectedRow(), 0).toString()+File.separator+copia,model.getValueAt(t.getSelectedRow(), 0).toString());
					}
					
				}
			});
			bi=new JButton("Crear informe");
			bi.setEnabled(false);
			bi.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					listaCopias=new ArrayList<>();
					listarDirectorio(ruta);
					if(listaCopias.size()>0){
						crearInforme();
					}else{
						JOptionPane.showMessageDialog(null, "No existen copias para realizar informe","Error",JOptionPane.ERROR_MESSAGE);
					}
					
				}
			});
			
			JPanel pb=new JPanel();
			pb.setLayout(new GridLayout(3, 1));
			pb.add(bc);
			pb.add(br);
			pb.add(bi);
			model=new DefaultTableModel();
			js=new JScrollPane();
			t=new JTable();
			JLabel lgrafico=new JLabel();
			t.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					bc.setEnabled(true);
					br.setEnabled(true);
					String copia=model.getValueAt(t.getSelectedRow(), 0).toString();
					File f = new File(ruta+copia+File.separator);
					br.removeAllItems();
					if (f.exists()){
						File[] ficheros = f.listFiles();
						if(ficheros.length<=0)
							br.addItem("No existen copias");
						br.addItem("Seleccionar Copia");
						for (int x=0;x<ficheros.length;x++){
							br.addItem(ficheros[x].getName());
						}
					}
					else {
						
						br.addItem("No existen copias");
					}
					CardLayout card=(CardLayout)sur.getLayout();
					card.show(sur, copia);
					sur.setVisible(true);
				}
			});
			sur=new JPanel(new CardLayout());
			sur.setVisible(false);
			getContentPane().setLayout(new BorderLayout());
			model.addColumn("Nombre");
			t.setModel(model);
			js.setViewportView(t);
			getContentPane().add(pb, BorderLayout.EAST);
			getContentPane().add(js,BorderLayout.CENTER);
			getContentPane().add(sur, BorderLayout.SOUTH);
			setSize(300, 300);
			setLocationRelativeTo(null);
			
			configurar.setEnabled(false);
			setJMenuBar(barra);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pack();
			setVisible(true);
		}catch(IOException e){
			JOptionPane.showMessageDialog(null,"Error al cargar xml","Mensaje de Error",JOptionPane.ERROR_MESSAGE);

		}

		
	}
	/**
	 * este metodo crea el informe con los datos del array
	 */
	public void crearInforme() {
		try {
			CopiasDatasource datasource = new CopiasDatasource(); 
			datasource.addCopias(listaCopias);
			JasperReport report;
			report = (JasperReport)JRLoader.loadObjectFromFile("reporTrabajo.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(report,null, datasource); 
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("repor.pdf"));
			exporter.exportReport();
			String [] botones = { "Si, con la aplicaci�n por defecto", " Si, con el visor de java", "No" };
			
			int respuesta = JOptionPane.showOptionDialog (null, "�Quiere abrir el informe?", "Informe creado", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, botones, botones[0]);
			if(respuesta==0){
				try {
					File p=new File("repor.pdf");
					Desktop.getDesktop().open(p);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Error al abrir el informe","Error",JOptionPane.ERROR_MESSAGE);
				}
			}else if(respuesta==1){
				JasperViewer.viewReport(jasperPrint);
			}
		} catch (JRException e) {
			JOptionPane.showMessageDialog(null, "Error al crear el informe","Error",JOptionPane.ERROR_MESSAGE);
		}
	}
	/*public void crearInforme(){
		JasperReport report = null;
        try {
            report = (JasperReport) JRLoader.loadObjectFromFile("reporTrabajo.jasper");
            JasperPrint jasperPrint = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(listaCopias));
            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File("reportPDFPuntos.pdf"));
            exporter.exportReport();

        } catch (JRException e1) {
            e1.printStackTrace();
        }
	}*/
	public static void main(String [] args){
		try {
			Ventana v=new Ventana();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	/**
	 * el metodo retorna un imagenIcon con un grafrico
	 * @param tablas
	 * @param bd
	 * @return
	 */
	public ImageIcon crearGrafico(ArrayList<Table> tablas,String bd){
		try{
			DefaultPieDataset pieDataset = new DefaultPieDataset();
			for (Table t : tablas) {
				pieDataset.setValue(t.getNombre(), t.getSize());
			}
			JFreeChart chart = ChartFactory.createPieChart(bd,pieDataset,true,true,false);
			PiePlot plot = (PiePlot) chart.getPlot();
			ChartUtilities.saveChartAsJPEG(new File("char"+bd+".jpg"), chart, 500, 300);
			ImageIcon img = new ImageIcon("char"+bd+".jpg");
			return img;
			} catch (Exception e) {
			return null;
			}
	}
	/**
	 * inicializa el hilo
	 */
	public void copias(){
		h=new Hilo(copiasS,ruta,pss,us);
		h.start();
	}
	/**
	 * llama al metodo crear grafico por cada base de datos
	 */
	public void crearGraficos(){
		for (String string : b) {
			JPanel car=new JPanel();
			JLabel lGrafico=new JLabel(crearGrafico(new Sql().listadoTablas(string,c), string));
			car.add(lGrafico);
			sur.add(car,string);
		}
	}
	/**
	 * lista las copias de seguridad para el informe
	 * @param rutaa
	 */
	public boolean listarDirectorio(String rutaa){
		File f = new File(rutaa);
		
		if(!f.exists()){
			return false;
		}else{
				File[] ficheros = f.listFiles();
				for (int x=0;x<ficheros.length;x++){
					if (ficheros[x].isDirectory()){
						listarDirectorio(ficheros[x]+"\\");
						
					}else{
						int i=(int)ficheros[x].length();
						Tablee t=new Tablee(ficheros[x].getName(),i);
						listaCopias.add(t);
					}
			}
			return true;
		}
	}

}
