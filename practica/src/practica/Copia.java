package practica;

/**
 * clae para definir las copias
 * @author borja
 *
 */

public class Copia {
	private String dia;
	private String base;
	private String hora;
	private String fecha;
	public Copia() {
		fecha="00 00 00";
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	

}
