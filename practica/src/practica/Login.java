package practica;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
/**
 * crea el dialogo de login
 * @author borja
 *
 */
public class Login extends JDialog {
	private String ip;
	private String user;
	private String pass;
	private JTextField ipt;
	private JTextField usuariot;
	private JPasswordField passt;
	
	public Login() {
		ventana();
	}
		
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	/**
	 * contructor que genera el dialogo
	 */
	public void ventana(){
		getContentPane().setLayout(new GridLayout(4, 2));
		JButton ba=new JButton("Aceptar");
		ba.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				aceptar();
				
			}
		});
		JButton bc=new JButton("Cancelar");
		bc.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelar();
				
			}
		});
		JLabel ipl=new JLabel("IP:");
		JLabel usuariol=new JLabel("Usuario:");
		JLabel passl=new JLabel("Pass:");
		ipl.setHorizontalAlignment(SwingConstants.CENTER);
		usuariol.setHorizontalAlignment(SwingConstants.CENTER);
		passl.setHorizontalAlignment(SwingConstants.CENTER);
		ipt=new JTextField("localhost");
	    usuariot=new JTextField("root");
		passt=new JPasswordField("");
		getContentPane().add(ipl);
		getContentPane().add(ipt);
		getContentPane().add(usuariol);
		getContentPane().add(usuariot);
		getContentPane().add(passl);
		getContentPane().add(passt);
		getContentPane().add(ba);
		getContentPane().add(bc);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
	            @Override
	            public void windowClosing(java.awt.event.WindowEvent evt) {
	                cancelar();
	            }
	        });
		setModal(true);
		setSize(200,125);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}
	/**
	 * metodo que es llamado cuando se acepta el dialogo
	 */
	public void aceptar(){
		ip=ipt.getText();
		user=usuariot.getText();
		pass=String.valueOf(passt.getPassword());
		setVisible(false);
		
	}
	/**
	 * metodo que es llamado cuando se cancela el dialogo
	 */
	public void cancelar() {
		System.exit(0);
	}
	

}
