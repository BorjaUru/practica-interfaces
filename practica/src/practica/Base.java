package practica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;
/**
 * clase que crea la carpeta y las copias de seguridad y restaura con estas
 * @author borja
 *
 */
public class Base {
	private String ruta;
	private String pass;
	private String us;
	
	
	public Base(String ruta, String pass,String us){
		this.pass=pass;
		this.ruta=ruta;
		this.us=us;
	}
	/**
	 * 
	 * @param carpeta
	 * @return
	 */
	public String crearCarpeta(String carpeta){
		String rutaCompleta=ruta+File.separator+carpeta;
		File folder = new File(rutaCompleta);
		if(!folder.exists()){
			folder.mkdir();
		}
		return rutaCompleta;
	}
	/**
	 * crea copias
	 * @param base
	 */
	public void copia(String base){
		try {
			Calendar c=Calendar.getInstance();
	    	GregorianCalendar g = new GregorianCalendar();
			String rutaCompleta=crearCarpeta(base+File.separator);
			int ano=c.get(g.YEAR);
			int dia=c.get(g.DAY_OF_MONTH);
			int mes=c.get(g.MONTH)+1;
			int hora=c.get(g.HOUR_OF_DAY);
			int min=c.get(g.MINUTE);
			int seg=c.get(g.SECOND);
            Process p = Runtime.getRuntime().exec("C:\\xampp\\mysql\\bin\\mysqldump -ump -u root "+pass+" "+ base);
            InputStream is = p.getInputStream();
            FileOutputStream fos = new FileOutputStream(rutaCompleta+base+"_"+dia+"-"+mes+"-"+ano+"("+hora+"-"+min+"-"+seg+").sql");
            byte[] buffer = new byte[1024];
            int leido = is.read(buffer);
            while (leido > 0) {
                fos.write(buffer, 0, leido);
                leido = is.read(buffer);
            }
            fos.close();
            is.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	/**
	 * restaura desde las copias
	 * @param bd
	 * @param nombre
	 */
	public void restaurarCopia(String bd,String nombre){
		try {
			
            Process p = Runtime.getRuntime().exec("C:\\xampp\\mysql\\bin\\mysql -u "+us+" "+pass + " "+ nombre);
            OutputStream os = p.getOutputStream();
            FileInputStream fis = new FileInputStream(ruta+bd);
            
            byte[] buffer = new byte[1000];

            int leido = fis.read(buffer);
            while (leido > 0) {
                os.write(buffer, 0, leido);
                leido = fis.read(buffer);
            }

            os.flush();
            os.close();
            fis.close();

        } catch (Exception e) {
          e.printStackTrace();

        }
	}
	
}
