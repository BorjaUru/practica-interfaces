package practica;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
/**
 * clase que crea y lee los ficheros de configuracion y de con las copias pendientes
 * @author borja
 *
 */
public class Fichero {
	private final String nombreArchivo="conf.pr";
	/**
	 * crea los propertis
	 * @param ruta
	 */
	public void crearModificarPropertis(String ruta){
		Properties propiedades = new Properties();
		OutputStream salida = null;
		try {
		   salida = new FileOutputStream(nombreArchivo);
		   propiedades.setProperty("ruta",ruta);
		   propiedades.store(salida,null);
		} catch (IOException io) {
		       io.printStackTrace();
		} finally {
		   if (salida != null) {
		       try {
		           salida.close();
		       } catch (IOException e) {
		            e.printStackTrace();
		       }
		  }
		}
	}
	/**
	 * lee las propertis
	 * @return
	 */
public String leerPropertis(){
	String ruta=System.getProperty("user.home")+File.separator+"copias"+File.separator;
	File f = new File(nombreArchivo);
	if(!f.exists()){
		crearModificarPropertis(ruta);
		return ruta;
	}
	Properties p = new Properties();
	try {
		p.load(new FileReader(nombreArchivo));
		Enumeration<Object> keys = p.keys();

		while (keys.hasMoreElements()){
		   Object key = keys.nextElement();
		   ruta=p.get(key).toString();
		}

	} catch (IOException e) {
		
		e.printStackTrace();
	}
	return ruta;
}
/**
 * guarda las copias pendientes
 * @return
 */
public ConcurrentHashMap<Copia,Copia> rf() {
	ConcurrentHashMap<Copia,Copia> copiasS=new ConcurrentHashMap<Copia,Copia>();
	File archivo = null;
    FileReader fr = null;
    BufferedReader br = null;
    try {
       archivo = new File ("prueba.txt");
       if(!archivo.exists()){
    	   return copiasS;
       }
       fr = new FileReader (archivo);
       br = new BufferedReader(fr);
       String linea;
       while((linea=br.readLine())!=null){
    	   Copia c=new Copia();
    	   c.setBase(linea);
    	   c.setDia(br.readLine());
    	   c.setFecha(br.readLine());
    	   c.setHora(br.readLine());
    	   copiasS.put(c, c);
    	   
       }
          
    }
    catch(Exception e){
       e.printStackTrace();
    }finally{
       try{                    
          if( null != fr ){   
             fr.close();     
          }                  
       }catch (Exception e2){ 
          e2.printStackTrace();
       }
    }
    return copiasS;
	
}
public void wf(ConcurrentHashMap<Copia,Copia> copiasS) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("prueba.txt",false);
            pw = new PrintWriter(fichero);
            for (Copia c : copiasS.values()) {
				pw.println(c.getBase());
				pw.println(c.getDia());
				pw.println(c.getFecha());
				pw.println(c.getHora());
			}

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
          
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
  
}

}
