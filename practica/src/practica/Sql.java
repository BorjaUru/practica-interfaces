package practica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
/**
 * clase para conectarse a mysql
 * @author borja
 *
 */
public class Sql {
	private Connection c;
	
	public Connection getC() {
		return c;
	}
	public void setC(Connection c) {
		this.c = c;
	}
	/**
	 * conexion con mysql
	 * @param i
	 * @param u
	 * @param p
	 * @return
	 */
	public boolean conectar(String i,String u,String p) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			JOptionPane.showMessageDialog(null, "Driver error", "Driver", JOptionPane.ERROR_MESSAGE);
			
			System.exit(0);
		}
        try {
			c = DriverManager.getConnection("jdbc:mysql://"+i,u,p);
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error Usuario o Pass incorrectos", "Error de login", JOptionPane.ERROR_MESSAGE);
			return false;
		}
        return true;
	}
	/**
	 * lista las bases de datos
	 * @param cc
	 * @return
	 */
	public ArrayList<String> listado(Connection cc){
		ArrayList<String>a = new ArrayList();
		PreparedStatement sentencia;
        try {
            sentencia = cc.prepareStatement("SELECT table_schema FROM information_schema.TABLES GROUP BY table_schema");
            ResultSet r= sentencia.executeQuery();
            while(r.next()){
				a.add(r.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
		
	}
	/**
	 * lista las bases de datos
	 * @param bd
	 * @param cc
	 * @return
	 */
	public ArrayList<Table> listadoTablas(String bd,Connection cc){
		ArrayList<Table> tablas=new ArrayList<>();
		PreparedStatement sentencia;
        try {
            sentencia = cc.prepareStatement("SELECT table_name Tabla,(data_length+index_length)/(1024*1024) Tama�o FROM information_schema.tables WHERE table_schema='"+bd+"'");
            ResultSet r= sentencia.executeQuery();
            while(r.next()){
            	String nombre=r.getString(1);
            	float size=r.getFloat(2);
            	Table t=new Table(nombre,size);
            	tablas.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tablas;
		
	}
}
