package practica;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
/**
 * data source para el informe
 * @author borja
 *
 */
public class CopiasDatasource implements JRDataSource{
	private List<Tablee> listaCopias;
	private int indiceCopias= -1;

	@Override
	public Object getFieldValue(JRField jrField) throws JRException {
		Object valor = null;
		 if(jrField.getName().equals("nombre")){
			 valor =listaCopias.get(indiceCopias).getNombre();
		 }
		 else if("tamano".equals(jrField.getName())){
			 valor =listaCopias.get(indiceCopias).getTamano();
		 }
		 return valor; 	}

	@Override
	public boolean next() throws JRException {
		 return ++indiceCopias < listaCopias.size();
	}
	public void addCopias(List copias){
	 this.listaCopias=copias;
	 }

}
