Copyright (c) <a�o>, <titular del copyright>
Todos los derechos reservados.

La redistribuci�n y el uso en las formas de c�digo fuente y binario, con o sin
modificaciones, est�n permitidos siempre que se cumplan las siguientes condiciones:

1. Las redistribuciones del c�digo fuente deben conservar el aviso de copyright
   anterior, esta lista de condiciones y el siguiente descargo de responsabilidad.

2. Las redistribuciones en formato binario deben reproducir el aviso de copyright
   anterior, esta lista de condiciones y el siguiente descargo de responsabilidad
   en la documentaci�n y/u otros materiales suministrados con la distribuci�n.

ESTE SOFTWARE SE SUMINISTRA POR <TITULAR DEL COPYRIGHT> ''COMO EST�'' Y CUALQUIER
GARANT�AS EXPRESA O IMPL�CITA, INCLUYENDO, PERO NO LIMITADO A, LAS GARANT�AS
IMPL�CITAS DE COMERCIALIZACI�N Y APTITUD PARA UN PROP�SITO PARTICULAR SON
RECHAZADAS. EN NING�N CASO <TITULAR DEL COPYRIGHT> O COLABORADORES SER�N
RESPONSABLES POR NING�N DA�O DIRECTO, INDIRECTO, INCIDENTAL, ESPECIAL, EJEMPLAR O
COSECUENCIAL (INCLUYENDO, PERO NO LIMITADO A, LA ADQUISICI�N O SUSTITUCI�N DE
BIENES O SERVICIOS; LA P�RDIDA DE USO, DE DATOS O DE BENEFICIOS; O INTERRUPCI�N
DE LA ACTIVIDAD EMPRESARIAL) O POR CUALQUIER TEOR�A DE RESPONSABILIDAD, YA SEA POR
CONTRATO, RESPONSABILIDAD ESTRICTA O AGRAVIO (INCLUYENDO NEGLIGENCIA O CUALQUIER
OTRA CAUSA) QUE SURJA DE CUALQUIER MANERA DEL USO DE ESTE SOFTWARE, INCLUSO SI SE
HA ADVERTIDO DE LA POSIBILIDAD DE TALES DA�OS.

Las opiniones y conclusiones contenidas en el software y la documentaci�n son las
de los autores y no deben interpretarse como la representaci�n de las pol�ticas
oficiales, ya sean expresas o impl�citas, de <titular del copyright>.